FROM jupyter/minimal-notebook

RUN conda install nltk=3.4.0
RUN conda install beautifulsoup4
RUN conda install feedparser
RUN conda install matplotlib
RUN conda install scipy

RUN ["python", "-c", "import nltk; nltk.download('all')"]
